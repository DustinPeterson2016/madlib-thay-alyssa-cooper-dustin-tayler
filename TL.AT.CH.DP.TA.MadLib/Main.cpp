// Thay, Alyssa, Cooper, Dustin, Tayler
// MadLib Group project

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void display(string input[11]);

int main()
{
	//array
	const int madlib = 11;
	string input[madlib] = {"a sport: ", "a city: ", "a person: ", "an action verb: ",
							"a vehicle: ", "a place: ", "a noun: ", "an adjective: ", 
							"a food: ", "a liquid: ", "an adjective: " };

	//collecting data loop
	for (int i = 0; i < madlib; i++)
	{
		cout << "Enter " << input[i];
		getline(cin, input[i]);
	}

	cout << "\n\n";

	display(input);

	char decision;

	//Ask to put into text file
	cout << "Would you like to save file? y/n: ";
	cin >> decision;

	if (decision == 'y' || decision == 'Y') //if yes statement
	{
		string filepath = "Madlib.txt";

		ofstream ofs(filepath);
		ofs << "One day my best friend and I decided to go to the " << input[0] << " in " << input[1] << "." << endl
			<< "We really wanted to see " << input[2] << " play." << endl
			<< "So we " << input[3] << " in the " << input[4] << " and headed down to the " << input[5] << " and bought some " << input[6] << "." << endl
			<< "We watched the game and it was " << input[7] << "." << endl
			<< "We ate some " << input[8] << " and drank some " << input[9] << "." << endl
			<< "We had a " << input[10] << " time, and can't wait to go again." << endl;

		ofs.close();

		cout << "\n\n";

		cout << "Madlib.txt has been created";

	}
	else
	{
		cout << "Goodbye!";
	}


	(void)_getch();
	return 0;
}

void display(string input[])
{
	cout << "One day my best friend and I decided to go to the " << input[0] << " in " << input[1] << "." << endl 
		 << "We really wanted to see " << input[2] << " play." << endl
		 << "So we " << input[3] << " in the " << input[4] << " and headed down to the " << input[5] << " and bought some " << input[6] << "." << endl
		 << "We watched the game and it was " << input[7] << "." << endl
		 << "We ate some " << input[8] << " and drank some " << input[9] << endl
		 << "We had a " << input[10] << " time, and can't wait to go again." << endl;
}
